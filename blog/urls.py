from django.urls import path

from . import views
# from .views import BlogImageView

urlpatterns = [
    path('', views.PostListView.as_view(), name='blog'),
    path('lord/upload', views.BlogImageUploadView.as_view(), name='blog-upload-image'),
    path('images/', views.BlogImageListView.as_view(), name='blog-images-list'),
    path('<int:blog_id>', views.blogShow, name='post-detail'),
]
