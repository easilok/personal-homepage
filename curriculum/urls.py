from django.urls import path

from . import views

urlpatterns = [
    path('', views.curriculum, name='curriculum'),
    path('<int:curriculum_id>', views.dowloadCurriculum, name='download-curriculum'),
]
