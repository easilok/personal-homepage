from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, Http404
from .models import Curriculum

# Create your views here.
def curriculum(request):
    curriculums = Curriculum.objects.all()
    context = {
        'curriculums': curriculums,
        'navMenu': 'curriculum'
    }
    return render(request, 'curriculum.html', context=context)

def dowloadCurriculum(request, curriculum_id):

    curriculum = get_object_or_404(Curriculum, pk=curriculum_id)

    try:
        pdf = open(curriculum.path, 'rb')
    except Exception:
        raise Http404("File does not exist")

    response = HttpResponse(pdf, content_type='application/pdf')
    # response = HttpResponse(curriculum.path, content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename="' + curriculum.filename + '"'

    curriculum.downloads = curriculum.downloads + 1;
    curriculum.save()

    return response
