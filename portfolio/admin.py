from django.contrib import admin
from .models import Project, ProjectTag, ProjectImage

# Register your models here.
admin.site.register(Project)
admin.site.register(ProjectTag)
admin.site.register(ProjectImage)
