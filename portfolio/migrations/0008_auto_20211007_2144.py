# Generated by Django 3.1.2 on 2021-10-07 21:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('portfolio', '0007_auto_20211007_1418'),
    ]

    operations = [
        migrations.RenameField(
            model_name='project',
            old_name='tag',
            new_name='tags',
        ),
        migrations.RemoveField(
            model_name='projectimage',
            name='project',
        ),
        migrations.AddField(
            model_name='project',
            name='images',
            field=models.ManyToManyField(to='portfolio.ProjectImage'),
        ),
    ]
