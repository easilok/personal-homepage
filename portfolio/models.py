from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import ugettext_lazy as _
from django.urls import reverse

class ProjectTag(models.Model):
    name = models.CharField(max_length=100, unique=True)
    slug = models.CharField(max_length=10, unique=True)
    color = models.CharField(max_length=10, blank=True)
    order = models.IntegerField(default=0)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['order', 'name']

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return ""

class ProjectImage(models.Model):
    title = models.CharField(max_length=100)
    image = models.FileField(upload_to='portfolio/')
    order = models.IntegerField(default=0)
    # project = models.ForeignKey(Project, on_delete=models.CASCADE)
    isThumbnail = models.BooleanField(default=False)

    class Meta:
        ordering = ['order']

    def __str__(self):
        return self.title

class Project(models.Model):
    title = models.CharField(max_length=100)
    subtitle = models.CharField(max_length=100)
    presentation = models.TextField()
    description = models.TextField()
    features = models.TextField()
    future = models.TextField()
    public = models.BooleanField(default=False)
    order = models.IntegerField(default=0)
    tags = models.ManyToManyField(ProjectTag)
    images = models.ManyToManyField(ProjectImage)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    PROJECT_LANGUAGE = (
        (0, 'en'),
        (1, 'pt'),
    )
    language = models.IntegerField(choices=PROJECT_LANGUAGE, default=0)

    class Meta:
        ordering = ['order', '-created_at']

    def get_absolute_url(self):
        return reverse('project-detail', args=[str(self.id)])

    def __str__(self):
        return self.title

