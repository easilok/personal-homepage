const { useState, useEffect, useCallback } = React;

const Slideshow = props => {
  const [imgIndex, setImgIndex] = useState(0);
  const [showModal, setShowModal] = useState(false);
  const [slideshowTimer, setSlideshowTimer] = useState();

  const restartTimer = useCallback(() => {
    if (slideshowTimer) {
      clearInterval(slideshowTimer);
    }
    setSlideshowTimer(setInterval(() => imageNavigationHandler(1), 5000));
  }, []);

  useEffect(() => {
    restartTimer();
    () => clearInterval(slideshowTimer);
  }, [restartTimer]);

  const imageNavigationHandler = useCallback((increment) => {
    if (props.images.length <= 1) {
      return;
    }
    // if (increment === 0) return;
    if (increment >= 0) {
      setImgIndex(prevState =>
        prevState >= props.images.length - 1 ? 0 : prevState + 1);
    } else {
      setImgIndex(prevState =>
        prevState <= 0 ? props.images.length - 1 : prevState - 1);
    }
    // restartTimer();
  }, [setImgIndex, restartTimer]);

  const imageClickHandler = (e) => {
    e.stopPropagation();
    setShowModal(true);
    const body = document.getElementById('body');
    body.style.overflow = 'hidden';
  };

  const imgSrc = props.images[imgIndex].path;
  const imgAlt = props.images[imgIndex].title;

  // const slideImageStyle = {
  //   backgroundImage: 'url(' + imgSrc + ')',
  // };

  return (
    <div className="project-data__slideshow">
      {/*
      <div className="project-data__slideshow-image" style={slideImageStyle}
        onClick={imageClickHandler}>
        <div className="project-data__slideshow-left"
          onClick={imageNavigationHandler.bind(this, -1)}><i className="fas fa-angle-left"></i></div>
        <div className="project-data__slideshow-right"
          onClick={imageNavigationHandler.bind(this, 1)}><i className="fas fa-angle-right"></i></div>
      </div>
      */}
      <img className="slideshow__image" src={imgSrc} alt={imgAlt} onClick={imageClickHandler} />
      {props.images.length > 1 &&
        <div className="project-data__slideshow-dots">
          <div className="dots-container">
            {props.images.map((img, i) => (
              <div className={`dot ${i === imgIndex ? 'active' : ''}`}
                key={i}
                onClick={(e) => {
                  e.stopPropagation();
                  setImgIndex(i);
                }}
              ></div>
            ))}
          </div>
        </div>
      }
      {showModal &&
        <ModalSlideshow imageList={props.images} startIndex={imgIndex}
          onClose={(e) => {
            e.stopPropagation();
            setShowModal(false);
            const body = document.getElementById('body');
            body.style.overflow = '';
          }} />
      }
    </div>
  );
}

const ProjectData = props => {
  const [tabSelected, setTabSelected] = useState('description');

  let menuBody;

  if (tabSelected === 'features') {
    menuBody = props.features;
  } else if (tabSelected === 'future') {
    menuBody = props.future;
  } else {
    menuBody = props.description;
  }

  return (
    <div className="project-data">
      <ul className="project-data__tab">
        <li className={`project-data__tab-item
          ${tabSelected === 'description' ? 'selected' : ''}`}
          onClick={() => setTabSelected('description')}>
          Description
        </li>
        <li className={`project-data__tab-item 
          ${tabSelected === 'features' ? 'selected' : ''}`}
          onClick={() => setTabSelected('features')}>
          Features
        </li>
        <li className={`project-data__tab-item 
          ${tabSelected === 'future' ? 'selected' : ''}`}
          onClick={() => setTabSelected('future')}>
          Future
        </li>
      </ul>
      <div className="project-data__body"
        dangerouslySetInnerHTML={{ __html: menuBody }}>
      </div>
    </div>
  );
}

function ModalSlideshow(props) {
  const { imageList } = props;
  const [imgIndex, setImgIndex] = useState(props.startIndex);

  const imageNavigationHandler = (increment, e) => {
    e.stopPropagation();
    // if (increment === 0) return;
    if (increment >= 0) {
      setImgIndex(prevState =>
        prevState >= imageList.length - 1 ? 0 : prevState + 1);
    } else {
      setImgIndex(prevState =>
        prevState <= 0 ? imageList.length - 1 : prevState - 1);
    }
  };

  const imageStyle = {
    backgroundImage: 'url(' + imageList[imgIndex].path + ')',
  };

  return (
    <div className="project-modal">
      <div className="project-modal__content">
        <div className="project-modal__header">
          <span className="project-modal__close" onClick={props.onClose}>&times;</span>
        </div>
        <div className="project-modal__body">
          {imageList.length > 1 &&
          <div className="project-modal__slideshow-left"
            onClick={imageNavigationHandler.bind(this, -1)}><i className="fas fa-angle-left"></i></div>
          }
          <div className="project-modal__image" style={imageStyle} >
          </div>
          {imageList.length > 1 &&
          <div className="project-modal__slideshow-right"
            onClick={imageNavigationHandler.bind(this, 1)}><i className="fas fa-angle-right"></i></div>
          }
        </div>
        <div className="project-modal__footer">
          {imageList.length > 1 &&
          <div className="dots-container">
            {imageList.map((img, i) => (
              <div className={`dot ${i === imgIndex ? 'active' : ''}`}
                key={i}
                onClick={(e) => {
                  e.stopPropagation();
                  setImgIndex(i);
                }}
              ></div>
            ))}
          </div>
          }
        </div>
      </div>
    </div>
  );
}

// const images = document.querySelectorAll('.project-images');

// const imagesData = [];
// images.forEach(i => imagesData.push({
//   path: i.getAttribute('src'),
//   title: i.getAttribute('alt'),
// }));

// const projectDescription = document.getElementById('project-description').innerHTML;
// const projectFeatures = document.getElementById('project-features').innerHTML;
// const projectFuture = document.getElementById('project-future').innerHTML;

const App = (props) => {
  const projectId = props.projectId;
  const [project, setProject] = useState();

  useEffect(() => {
    fetch('/api/portfolio/' + projectId)
      .then(data => data.json())
      .then(dataRes => {
        setProject(dataRes);
      })
      .catch(error => console.log(error));
  }, [projectId]);

  let imagesData = [];
  let projectDescription = '';
  let projectFeatures = '';
  let projectFuture = '';
  if (project) {
    imagesData = project.images.map(i => {
      return {
        path: i.image,
        title: i.title,
      }
    });
    projectDescription = project.description;
    projectFeatures = project.features;
    projectFuture = project.future;
  }

  return (
    <div>
      {imagesData.length > 0 &&
        <Slideshow images={imagesData} />
      }
      <ProjectData
        description={projectDescription}
        features={projectFeatures}
        future={projectFuture}
      />
    </div>
  );
}


const rootElement = document.getElementById('project-app');
ReactDOM.render(<App {...rootElement.dataset} />, rootElement);

