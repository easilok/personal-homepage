from django.urls import path, include

from . import views

urlpatterns = [
    path('', views.PortfolioListView.as_view(), name='portfolio'),
    path('<int:project_id>', views.projectShow, name='project-detail'),
    # path('', views.portfolioList, name='portfolio'),
]
