from django.shortcuts import render, get_object_or_404
from portfolio.models import Project, ProjectTag, ProjectImage
from django.views import generic

# Create your views here.
def portfolioList(request):

    # post = get_object_or_404(Post, pk=blog_id)
    # post.tags = post.tag.all()
    # post.categories = post.category.all()

    context = {
        'projects': ['First Project'],
        'navMenu': 'portfolio'
    }

    return render(request, 'portfolio.html', context=context)

class PortfolioListView(generic.ListView):
    model = Project
    context_object_name = 'projects'
    # template_name = 'portfolio.html'
    template_name = 'portfolio-circle.html'

    def get_context_data(self, **kwargs):
        context = super(PortfolioListView, self).get_context_data(**kwargs)
        userLanguage = self.request.LANGUAGE_CODE
        filterLanguage = 0
        
        for language in Project.PROJECT_LANGUAGE:
            if language[1] == userLanguage:
                filterLanguage = language[0]
                break

        if self.request.user.is_authenticated:
            # projects = Project.objects.filter(language = filterLanguage)
            # For now, loggedin user sees all
            projects = Project.objects.all()
        else:
            projects = Project.objects.filter(public = True, language = filterLanguage)

        for project in projects:
            project.tagList = project.tags.all()
            project.thumbnail = project.images.filter(isThumbnail = True).first()
            print(project.thumbnail.image)

        context['projects'] = projects
        context['navMenu'] = 'portfolio'

        return context

def projectShow(request, project_id):

    project = get_object_or_404(Project, pk=project_id)
    project.tagList = project.tags.all()
    project.imageList = project.images.all()

    context = {
        'project': project,
        'navMenu': 'portfolio'
    }

    return render(request, 'project-show.html', context=context)


