from rest_framework import serializers

from portfolio.models import Project, ProjectImage, ProjectTag

class ProjectTagSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProjectTag
        fields = ('name', 'slug', 'color', 'order')

class ProjectImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProjectImage
        fields = ('title', 'image', 'order', 'isThumbnail')

class ProjectSerializer(serializers.ModelSerializer):
   images = ProjectImageSerializer(many = True, read_only = True)
   tags = ProjectTagSerializer(many = True, read_only = True)
   class Meta:
       model = Project
       fields = ('title', 'subtitle', 'presentation', 'description', 'features', 'future', 'tags', 'order', 'images')


