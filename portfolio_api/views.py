from django.shortcuts import render
from rest_framework import viewsets
from portfolio.models import Project, ProjectTag, ProjectImage
from portfolio_api.serializers import ProjectSerializer

class ProjectListSet(viewsets.ModelViewSet):
    """
    API endpoint that shows all projects
    """
    # queryset = Project.objects.filter(public = True)
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer

